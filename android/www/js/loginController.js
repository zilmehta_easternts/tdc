lsdApp.controller('loginController', function($scope,$rootScope,$location,$window,$http,$route)
{
		$rootScope.header(true);
		$rootScope.title("Login");
		
		if($rootScope.$storage.login1 == 1)
		{
			$location.path('/home');
		}
		
		$scope.formInfo = {};
		
		$scope.submitTheForm = function(item, event) 
		{
			if($rootScope.checkConnection())
			{	
				$scope.loader = true;
				var serdata = serialize($scope.formInfo);
				console.log(serdata);
				$http({
				url: serviceurl+"login.php",
				method: "POST",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				data: serdata
				}).success(function(data, status, headers, config) {
					if(data.status == '1'){
						$scope.loader = false;
						$rootScope.$storage.login1  = 1;
						$rootScope.$storage.username = data.name;
						 if($rootScope.allcategories === null) {
							  $http.get(serviceurl+'allcategories.php')
								.success(function (data) {
									$rootScope.allcategories = data.Categories;	 		
								})
								.error(function (data, status, headers, config) {   
								});		
						 }
						
						$location.path("/home") ;
					}else{	
						navigator.notification.alert('Invalid Login', function(){}, "Failure", "");
						//alert('Invalid Login');
					}			  	
				}).error(function(data, status, headers, config) {	
					//navigator.notification.alert('Submitting form failed!', function(){}, "Failure", "");	
				});	   
			}	
        }
		
	  document.addEventListener("backbutton", onBackKeyDown, false); 
});
