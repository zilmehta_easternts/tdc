lsdApp.controller('allordersController', function($scope,$http,$route,$window,$rootScope,$location) 
{
	$rootScope.title("Orders");
	if($rootScope.checkConnection())
	{
		$scope.loader = true;
		
		
		$scope.orders = '';
		
		var dataObject = {
			order_date: $rootScope.deliverydate
		};
		
		var serdata = serialize(dataObject);
		console.log(serdata);
		
		$http({
			url: serviceurl +"allorders.php",
			method: "POST",
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			},
			data: serdata
		}).success(function (data, status, headers, config) {
			console.log(data);
			$scope.orders = data;
			$scope.loader = false;
		}).error(function (data, status, headers, config) {
			
		});
	}
	
});
