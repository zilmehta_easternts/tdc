lsdApp.controller('shoppingcartController', function($scope,$http,$route,$window,$rootScope,$location) {
    $scope.cart = [];
    $scope.cartoptions = [];
    $scope.detailform = false;
    $rootScope.title("Cart");
    $scope.currentcatid = 0;
    var cartids = [];
    var cartimages = [];
  	$scope.optns = [];
  	$scope.totalprice = 0;
  	$scope.finalamt = 0;
  	$scope.advance_amount = 0;
  	$scope.discount = 0;
  	$scope.left_amount = 0;
  	$scope.noproducts = false;
  	$scope.credit_amount = 0;
  	$scope.delivery_time = '';
  	$scope.delivery_address = '';
  	$scope.showdiscount = false;
  	$scope.loader  = false;
  	$scope.branch = 'Parle Point';
  	
	db.transaction(function (tx)
	{
		tx.executeSql('SELECT * FROM customer_cart cc order by c_id asc', [], function (tx, results)
		{
				
			if(results.rows.length == 0)
			{
			
				$scope.$apply(function() {
					$scope.noproducts = true;
				});
			}
			
			for(var i=0; i<results.rows.length;i++)
			{
				var cartprod = {};
				cartprod = results.rows.item(i);
				cartids.push(results.rows.item(i).id);
				cartimages.push(results.rows.item(i).image_url);
				console.log(cartprod.id);
				$scope.totalprice = parseFloat($scope.totalprice) + parseFloat(results.rows.item(i).total_price);
				$scope.$apply(function() {
					$scope.cart.push(cartprod);
				});
			}	
			
			console.log(cartimages);
			
			 
		}, null);
		
	});
	
	//$scope.totalprice = '11000';
	db.transaction(function (tx1)
	{	
		for(i=0;i< cartids.length;i++)
		{
			console.log("SELECT * FROM customer_cart_options where cart_id = " + cartids[i]);
			tx1.executeSql('SELECT * FROM customer_cart_options where cart_id = ? ', [cartids[i]], function (tx1, results)
			{		
				var opt = [];
				if(results.rows.length > 0)
				{
					for(var j=0; j< results.rows.length; j++)
					{
						console.log(results.rows.item(j));
						$scope.info = {};
						$scope.info['cart_id'] = results.rows.item(j).cart_id;
						$scope.info['option_id'] = results.rows.item(j).option_id;
						$scope.info['option_name'] = results.rows.item(j).option_name;
						$scope.info['option_price'] = results.rows.item(j).option_price;
						$scope.info['other'] = results.rows.item(j).other;
						$scope.info['oqty'] = results.rows.item(j).oqty;
						opt.push($scope.info);	
					}	
				}	
				$scope.$apply(function() 
				{
						$scope.optns.push(opt);
				});
				
			}, null);
		}
		
	});
	
	$scope.updatecat = function(current) 
	{
		$scope.currentcatid = current;
	};
	
	$scope.updateimg = function(current,img) 
	{
		image = document.getElementById('myImage_' + current);
		image.src = img;
		
	};
	
	
	$scope.confirmorder = function()
	{
		if($rootScope.checkConnection())
		{
			$scope.loader = true;
			
			var dataObject = {
				customer_id : $rootScope.$storage.customer_id
			};
		
			var serdata = serialize(dataObject);
		
			console.log(serdata);
		
			$http({
				url: serviceurl +"getcredit.php",
				method: "POST",
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				},
				data: serdata
			}).success(function (data, status, headers, config) 
			{
				console.log(data);
				$rootScope.$storage.has_credit = data['has_credit'];
				$rootScope.$storage.credit_amount = data['credit_balance'];
				
				if($scope.totalprice >= 10000)
				{
					$scope.showdiscount = true;
					$scope.discount = parseFloat((10 * $scope.totalprice)/100);
				}
				else
				{
					$scope.showdiscount = false;
					$scope.discount = 0;
				}
			
				if($rootScope.$storage.has_credit == 'Y')
				 {
					 
					 if($scope.totalprice >= 10000)
					 {
						 /* if discount then check for totalprice - discount price*/
						 var sub = $scope.totalprice - $scope.discount;
						 if($rootScope.$storage.credit_amount >= sub)
						 {
								$scope.credit_amount = parseFloat(sub); 
						 }
						 else
						 {
								$scope.credit_amount = parseFloat($rootScope.$storage.credit_amount);
						 }
					 }
					 else
					 {
						 if($rootScope.$storage.credit_amount >= $scope.totalprice)
						 {
								$scope.credit_amount = parseFloat($scope.totalprice);
						 }
						 else
						 {
								$scope.credit_amount = parseFloat($rootScope.$storage.credit_amount);
						 }
					}
					  $scope.finalamt = parseFloat($scope.totalprice - $scope.credit_amount - $scope.discount);
				 }
				 else
				 {
					
					 $scope.finalamt = parseFloat($scope.totalprice - $scope.discount);
				 }
			
				if($scope.finalamt < 0)
				{
					$scope.finalamt = 0;
				}
				$scope.left_amount = parseFloat($scope.finalamt - $scope.advance_amount);
				
				$scope.loader = false;
				$scope.detailform = true;
			}).error(function (data, status, headers, config) {
				
			});
		}
	};
	
	
	
	$scope.calculatediscount = function()
	{
		 if($rootScope.$storage.has_credit == 'Y')
		 {
			 /* if discount then check for totalprice - discount price*/
			 var sub = $scope.totalprice - $scope.discount;
			 if(sub < 0)
			 {
				 alert('Discount cannot be greater than total price');
				 $scope.discount = parseFloat((10 * $scope.totalprice)/100);
				 return;
			 }
			 
			 if($rootScope.$storage.credit_amount >= sub)
			 {
					$scope.credit_amount = parseFloat(sub);
			 }
			 else
			 {
					$scope.credit_amount = parseFloat($rootScope.$storage.credit_amount);
			 }
			 
			  $scope.finalamt = parseFloat($scope.totalprice - $scope.credit_amount - $scope.discount);
		 }
		 else
		 {
			 $scope.finalamt = parseFloat($scope.totalprice - $scope.discount);
		 }
		
		if($scope.finalamt < 0)
		{
			$scope.finalamt = 0;
		}
		$scope.left_amount = parseFloat($scope.finalamt - $scope.advance_amount);
	};
	
	function onConfirm(buttonIndex)
	{
        var randno = Math.floor(Math.random()*9000) + 1000;
		if(buttonIndex == 1) 
		{
			$scope.loader = true;
		
		
				// Put navigator.confirm
				var productsid = '';
				var quantities = '' ;
				var categoriesid = '' ;
				for(i=0;i< $scope.cart.length;i++)
				{
					console.log("i:" + $scope.optns[i]);
					
					var imgname = $scope.cart[i].image_url;
					console.log("imgname:"+imgname);
					var fileName = '';
					if(imgname != '')
					{
						fileName = randno + i +'-' + imgname.substr(imgname.lastIndexOf('/') + 1);
						console.log("fileName:"+fileName);
					}
					
					
					
					if(productsid == '')
					{
						productsid 		= $scope.cart[i].p_id ;		
						quantities 		= $scope.cart[i].qty ;
						categoriesid 	= $scope.cart[i].c_id ;
						hasoptions 		= $scope.cart[i].has_options;
						flatprice 		= $scope.cart[i].flat_price;
						weight 			= $scope.cart[i].weight;
						ptypeid 		= $scope.cart[i].ptype_id;
						ptypename 		= $scope.cart[i].ptype_name;
						pprice 			= $scope.cart[i].pprice;
						ftypeid 		= $scope.cart[i].ftype_id;
						ftypename 		= $scope.cart[i].ftype_name;
						fprice 			= $scope.cart[i].fprice;
						amt 			= $scope.cart[i].amount;
						addamt 			= $scope.cart[i].add_amt;
						totalamt 		= $scope.cart[i].total_price;
						specification 	= $scope.cart[i].specification;
						image 			= fileName;
						
						newopt = '';
						for(k=0;k<$scope.optns[i].length;k++)
						{
								newopt = newopt + $scope.optns[i][k].option_id +"-" +$scope.optns[i][k].option_name + '-' + $scope.optns[i][k].option_price + '-' + $scope.optns[i][k].oqty +  '-' + $scope.optns[i][k].other + ",";
						}
						
						newopt = newopt.substring(0, newopt.length-1);
						
						
						totaloptions = newopt;
						
						console.log("Opt:"+ totaloptions);
					}
					else
					{
						productsid 		= productsid + ',' + $scope.cart[i].p_id ;		
						quantities 		= quantities + ',' + $scope.cart[i].qty ;
						categoriesid 	= categoriesid + ',' + $scope.cart[i].c_id ;
						hasoptions 		= hasoptions + ',' + $scope.cart[i].has_options;
						flatprice 		= flatprice + ',' + $scope.cart[i].flat_price;
						weight 			= weight + ',' + $scope.cart[i].weight;
						ptypeid 		= ptypeid + ',' + $scope.cart[i].ptype_id;
						ptypename 		= ptypename + ',' + $scope.cart[i].ptype_name;
						pprice 			= pprice + ',' + $scope.cart[i].pprice;
						ftypeid 		= ftypeid  + ','+ $scope.cart[i].ftype_id;
						ftypename 		= ftypename  + ','+ $scope.cart[i].ftype_name;
						fprice 			= fprice  + ','+ $scope.cart[i].fprice;
						amt 			= amt + ',' + $scope.cart[i].amount;
						addamt 			= addamt + ',' + $scope.cart[i].add_amt;
						totalamt 		= totalamt + ',' + $scope.cart[i].total_price;
						specification 	= specification + ',' + $scope.cart[i].specification;
						image 			= image + ',' + fileName;
						
						newopt = '';
						for(k=0;k<$scope.optns[i].length;k++)
						{
								newopt = newopt + $scope.optns[i][k].option_id +"-" +$scope.optns[i][k].option_name + '-' + $scope.optns[i][k].option_price + '-' + $scope.optns[i][k].oqty +  '-' + $scope.optns[i][k].other + ",";
						}
						
						newopt = newopt.substring(0, newopt.length-1);
						
						totaloptions = totaloptions + ':' + newopt;
						console.log("Opt1:"+ totaloptions);
					}
				}
				var creditused = 0;
				
				
				if((parseInt($scope.credit_amount) == 0) || $scope.credit_amount < 0)
				{
					
					creditused = 0;
				}
				else
				{
					creditused = 1;
				}
				
				
				var dataObject = 
				{
					  customer_id 	: $rootScope.$storage.customer_id,
					  deliverydate 	: $rootScope.$storage.order_deliverydate,
					  username		: $rootScope.$storage.username,
					  deliverytime 	: $scope.delivery_time,
					  deliveryadd 	: $scope.delivery_address,
					  products 		: productsid,
					  pquantities 	: quantities , 
					  pcategoriesid	: categoriesid , 
					  hasoptions 	: hasoptions,
					  totaloptions 	: totaloptions,
					  flatprice 	: flatprice,
					  weight    	: weight,
					  ptypeid 		: ptypeid,
					  ptypename     : ptypename,
					  pprice 		: pprice,
					  ftypeid 		: ftypeid,
					  ftypename 	: ftypename,
					  fprice 		: fprice,
					  amt 			: amt,
					  addamt		: addamt,
					  totalamt		: totalamt,
					  subtotal		: $scope.totalprice,
					  discount		: $scope.discount,
					  total			: $scope.finalamt,
					  advanceamt	: $scope.advance_amount,
					  leftamt		: $scope.left_amount,
					  creditused	: creditused,
					  creditid		: '0',
					  creditamt		: $scope.credit_amount,
					  specification :specification,
					  images		: image,
					  branch		: $scope.branch
				};
				   
			   console.log(dataObject);
			   var serdata = serialize(dataObject);
			   console.log(serdata);
			   
			   for(var j=0;j<cartimages.length;j++)
			   {
				   console.log("cartimages[j]:"+cartimages[j]);
				   if(cartimages[j] != '')
				   {
						var options = new FileUploadOptions();
						
						options.fileKey = "file";
						options.fileName = randno + j +'-' + cartimages[j].substr(cartimages[j].lastIndexOf('/') + 1);
						options.mimeType = "image/jpeg";
						options.params = {}; // if we need to send parameters to the server request
						console.log("options.fileName:"+options.fileName);
						var ft = new FileTransfer();
						ft.upload(cartimages[j], encodeURI("http://thedoughcompany.in/data1/uploadimg.php"), win, fail, options);
					}
			   }
			   
				$http({
					 url: serviceurl + "checkout.php",
					 method: "POST",
					 headers: {'Content-Type': 'application/x-www-form-urlencoded','Access-Control-Allow-Origin':'*'},
					 data: serdata
					}).success(function(data, status, headers, config) {
					if(data.msg == 1)
					{
						$rootScope.$storage.customer_id = 0;
						$rootScope.$storage.customer_name = '';
						$rootScope.deliverydate = '';
						$rootScope.orders = '';
						$rootScope.$storage.credit_amount = '0';
						$rootScope.$storage.has_credit = 'N';
						$rootScope.$storage.order_deliverydate = ''; 
						$scope.loader  = false;
						db.transaction(function (tx)
						{
							tx.executeSql('DELETE FROM customer_cart', []);
						});	
					
						db.transaction(function (tx)
						{
							tx.executeSql('DELETE FROM customer_cart_options', []);
						});	
						
						$rootScope.resetcart();
						$route.reload();
						
						//alert('Order has been successfully placed. : Order ID - ' + data.orderid);
						navigator.notification.alert('Order has been successfully placed. : Order ID - ' + data.orderid, function(){}, "Success", "");
						
					}
					else 
					{
						//$scope.message = "There occured some problem in placing the order..Please try agin later. "
						navigator.notification.alert('There occured some problem in placing the order..Please try again later.', function(){}, "Error", "");
					}
					
					
					}).error(function(data, status, headers, config) {
					navigator.notification.alert('There occured some problem in placing the order..Please try again later.', function(){}, "Error", "");
							
				});
		
		} 
	}
	
	$scope.checkout = function(item, event) 
	{
		if($rootScope.checkConnection())
		{
			if($scope.advance_amount.length == 0 ||  $scope.left_amount.length == 0 || $scope.discount.length == 0 || $scope.finalamt.length == 0 || $scope.delivery_time.length == 0)
			{ 
				//alert('Please fill up the data');	
				navigator.notification.alert('Please fill up the data.', function(){}, "Error", "");
			}
			else
			{
				navigator.notification.confirm(
					'Advance Amount : Rs.' + $scope.advance_amount + '\n' + 'Balance Amount : Rs.' + $scope.left_amount, 
					 onConfirm, // <-- no brackets
					'Place Order ?',
					['Ok','Cancel']
				);
			}
		}
		
	}
	
	document.removeEventListener("backbutton", onBackKeyDown, false); 
            
});
