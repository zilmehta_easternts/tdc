lsdApp.controller('pendingorderdetailController', function($scope,$http,$route,$window,$rootScope,$location,$routeParams) 
{
	$scope.orderid = $routeParams.id;
	$rootScope.title("Order No " + $scope.orderid);
	$scope.specificationdiv = false;
	if($rootScope.checkConnection())
	{
		$scope.loader = true;
		
		$scope.credit = 0;
		$scope.showcredit = false;
		$scope.selectedtype  = 'ready';
		$scope.showamount = false;
		$scope.remainingamt = 0;
		
		$scope.orderdetail = null;
		var dataObject = {
			order_id: $scope.orderid
		};
		
		var serdata = serialize(dataObject);
		
		console.log(serdata);
		
		$http({
			url: serviceurl +"orderdetail.php",
			method: "POST",
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			},
			data: serdata
		}).success(function (data, status, headers, config) 
		{
			$scope.orderdetail = data;
			$scope.loader = false;
			$scope.totalcredit = $scope.orderdetail.credit_balance;
			
			if(parseFloat($scope.orderdetail.credit_balance) >= parseFloat($scope.orderdetail.left_balance))
			{
				
				$scope.credit = parseInt($scope.orderdetail.left_balance);
				$scope.remainingamt = 0;
			}
			else
			{
				$scope.credit = parseInt($scope.orderdetail.credit_balance);
				$scope.remainingamt = $scope.orderdetail.left_balance - $scope.orderdetail.credit_balance ;
			}
			
		}).error(function (data, status, headers, config) {
			
		});
	}
	
	
	$scope.showamt = function()
	{
		if($scope.selectedtype == 'complete')
		{
			$scope.showamount = true;
			if($scope.credit == '0')
			{
				$scope.showcredit = false;
			}
			else
			{
				$scope.showcredit = true;
			}
		}
		else 
		{
			$scope.showamount = false;	
		}
		
	};
	
	$scope.submitTheForm = function()
	{
		if($rootScope.checkConnection())
		{
			$scope.loader = true;
			var dataObject = 
			{
				order_id: $scope.orderid,
				status	: $scope.selectedtype,
				amount	: $scope.remainingamt,
				credit : $scope.credit,
				advanceamount : $scope.orderdetail.advance_payment
			};
	
			var serdata = serialize(dataObject);
			console.log(serdata);
			
			$http({
				url: serviceurl +"changeorder.php",
				method: "POST",
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				},
				data: serdata
			}).success(function (data, status, headers, config) {
				
				if(data.msg == 1)
				{
					$scope.loader = false;
					$location.path('/pendingorders');
                    navigator.notification.alert('Successfully changed', function(){}, "Success", "");
					//alert('Successfully changed');
				}
			}).error(function (data, status, headers, config) {
				
			});
		}
	};
	
	
	$scope.specification = '';
	$scope.showspecification = function(opid,index)
	{
		$scope.specificationdiv = true;
		$scope.specification = $scope.orderdetail.products[index].specification;
		$scope.oproductid	= opid;
		$scope.ind	= index;
	};
	
	$scope.hidespecification = function()
	{
		$scope.specificationdiv = false;
	};
	
	$scope.submitspecification = function(oproductid,index)
	{
		if($rootScope.checkConnection())
		{
			$scope.loader = true;
			var dataObject = 
			{
					order_product_id: oproductid,
					specification	: $scope.specification
			};
		
			var serdata = serialize(dataObject);
			console.log(serdata);
			
			
			$http({
				url: serviceurl +"changespecification.php",
				method: "POST",
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				},
				data: serdata
			}).success(function (data, status, headers, config) {
				if(data.msg == 1)
				{
					$scope.loader = false;
					$scope.specificationdiv = false;
					$scope.orderdetail.products[index].specification = $scope.specification;
					//alert('Successfully Edited');
                    navigator.notification.alert('Successfully Edited', function(){}, "Success", "");
				}
			
			}).error(function (data, status, headers, config) {
				
			});
			
		}
	}
	
	document.removeEventListener("backbutton", onBackKeyDown, false); 
});
