lsdApp.controller('creditnoteController', function($scope,$rootScope,$location,$window,$http,$sce,$q,$modal,$log,$route) 
{
	$rootScope.title("Credit Note");
	
	if($rootScope.checkConnection())
	{
		$scope.loader = true;
		$scope.customer = '';
		$scope.type = 'c';
		
		$rootScope.customername = '';
		$rootScope.customerid = '0';
		$rootScope.mobile = '';
		
		$http.get(serviceurl + 'customers.php')
			 .success(function (data) {
				 $scope.customers = data.Customers;	 
					console.log($scope.customers);		
				 $scope.loader = false;
				})
			 .error(function (data, status, headers, config) {   
		  });
	 }
		  
	/* auto search code */  
	$scope.formInfo = {};
    $scope.dirty = {};
     
	function highlight(str, term) {
	  var highlight_regex = new RegExp('(' + term + ')', 'gi');
	  return str.replace(highlight_regex,
		'<span class="highlight">$1</span>');
	};

	function suggest_users(term) {
		if (term.length < 4)
			return;
	   var q = term.toLowerCase().trim(),
		   results = [];

	   for (var i = 0; i < $scope.customers.length; i++) {
		 var customer = $scope.customers[i];
		 if (customer.customers_name.toLowerCase().indexOf(q) !== -1 ||
			 customer.customers_mobile.toLowerCase().indexOf(q) !== -1)
		   results.push({
			 value: customer.customers_mobile,
			 // Pass the object as well. Can be any property name.
			 obj: customer,
			label: $sce.trustAsHtml(
				   '<div class="row nomargin">' +
				   ' <div class="col-xs-12">' +
				   '  <div class="pull-right" style="padding : 0px 15px;"><i class="fa fa-user">&nbsp;&nbsp;</i> <strong>'+highlight(customer.customers_name,term) + '</strong></div>' +
				   '  <i class="fa fa-phone">&nbsp;&nbsp;</i><strong>' + highlight(customer.customers_mobile,term) + '</strong> '+ 
				   ' </div>' +
				   '</div>'
				 )
		   });
	   }
	  
	   return results;
	};

	$scope.ac_options_users = {
	  suggest: suggest_users,
	  on_select: function (selected) {
		$scope.selected_customer = selected.obj;
		$rootScope.customername = $scope.selected_customer.customers_name;
		$rootScope.customerid 	= $scope.selected_customer.customers_id;
		$rootScope.mobile 	= $scope.selected_customer.customers_mobile;
		document.activeElement.blur(); // close keypad
	  }
	};    
     /* end of auto search code*/ 
     
    function onConfirm(buttonIndex)
	{
		if(buttonIndex == 1) 
		{
			var dataObject = 
			{
				amount: $scope.formInfo.amount,
				customer_id : $rootScope.customerid,
				customer_name : $rootScope.customername,
				customer_mobile : $rootScope.mobile,
				amount_type : $scope.type
			};
				
			var serdata = serialize(dataObject);
			console.log(serdata);
			
			$http({
			url: serviceurl + "addcredit.php",
			method: "POST",
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			},
			data: serdata
			}).success(function (data, status, headers, config) {
				if(data.msg == 1)
				{
					//alert('Successfully Added');
                    navigator.notification.alert('Successfully Added', function(){}, "Success", "");
				}
				else
				{
					//alert('There occured some problem .Please Try Again Later');
                    navigator.notification.alert('There occured some problem .Please Try Again Later', function(){}, "Error", "");
				}
				$route.reload();
			}).error(function (data, status, headers, config) {
				
			});

		}
	}
     
    $scope.submitTheForm = function()
	{
		if($rootScope.checkConnection())
		{
			var customerno = $scope.dirty.value;
			customerno = customerno.trim();
			if(customerno.length == 0 || $rootScope.customerid == 0)
			{
				//alert('Not a valid Mobile No.');
				navigator.notification.alert('Not a valid Mobile No.', function(){}, "Error", "");
			}
			else
			{
				navigator.notification.confirm(
					' Credit / Debit ', 
					 onConfirm, // <-- no brackets
					'Do you want to proceed ?',
					['Ok','Cancel']
				);
			}
		}
	};
	document.removeEventListener("backbutton", onBackKeyDown, false); 
});
