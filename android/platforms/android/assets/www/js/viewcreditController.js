lsdApp.controller('viewcreditController', function($scope,$http,$route,$window,$rootScope,$location,$routeParams,$sce,$q,$modal,$log) 
{
	$rootScope.title("View Credits");
	if($rootScope.checkConnection())
	{
		$scope.loader = true;
		$scope.showhistory = false;
		$scope.customername = '';
		$scope.customerid = 0;
		$scope.customerno = '';
		$scope.customers = null;
		
		$http.get(serviceurl + 'customers.php')
			 .success(function (data) {
				 $scope.customers = data.Customers;	 
				 console.log($scope.customers);		
				 $scope.loader = false;
				})
			 .error(function (data, status, headers, config) {   
		});
	}
    
    /* auto search code */  
    $scope.dirty = {};
     
	function highlight(str, term) {
	  var highlight_regex = new RegExp('(' + term + ')', 'gi');
	  return str.replace(highlight_regex,
		'<span class="highlight">$1</span>');
	};

	function suggest_users(term) {
		if (term.length < 4)
			return;
	   var q = term.toLowerCase().trim(),
		   results = [];

	   for (var i = 0; i < $scope.customers.length; i++) {
		 var customer = $scope.customers[i];
		 if (customer.customers_name.toLowerCase().indexOf(q) !== -1 ||
			 customer.customers_mobile.toLowerCase().indexOf(q) !== -1)
		   results.push({
			 value: customer.customers_mobile,
			 // Pass the object as well. Can be any property name.
			 obj: customer,
			  label: $sce.trustAsHtml(
				   '<div class="row nomargin">' +
				   ' <div class="col-xs-12">' +
				   '  <div class="pull-right" style="padding : 0px 15px;"><i class="fa fa-user">&nbsp;&nbsp;</i> <strong>'+highlight(customer.customers_name,term) + '</strong></div>' +
				   '  <i class="fa fa-phone">&nbsp;&nbsp;</i><strong>' + highlight(customer.customers_mobile,term) + '</strong> '+ 
				   ' </div>' +
				   '</div>'
				 )
		   });
	   }
	  
	   return results;
	};

	$scope.ac_options_users = {
	  suggest: suggest_users,
	  on_select: function (selected) {
		$scope.selected_customer = selected.obj;
		$scope.customername = $scope.selected_customer.customers_name;
		$scope.customerid 	= $scope.selected_customer.customers_id;
		$scope.customerno 	= $scope.selected_customer.customers_mobile;
		console.log($scope.selected_customer);
		document.activeElement.blur(); // close keypad
	  }
	};    
     /* end of auto search code*/ 
    
    $scope.submitTheForm = function()
    {
		if($rootScope.checkConnection())
		{
			console.log($scope.dirty.value);
			if($scope.customerid == 0)
			{
					alert('Mobile No. does not exist.');
					return;
			}
			$scope.loader = true;
			if($scope.dirty.value == '' || typeof($scope.dirty.value) == 'undefined')
			{
				//alert('Please Select Customer');
				navigator.notification.alert('Please Select Customer', function(){}, "Error", "");
			}
			else
			{
				var dataObject = 
				{
					customer_id : $scope.customerid,
				};
				var serdata = serialize(dataObject);
				console.log(serdata);
				
				$http({
				url: serviceurl + "viewcredits.php",
				method: "POST",
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				},
				data: serdata
				}).success(function (data, status, headers, config) {
					$scope.creditlist = data.creditlist;
					$scope.credit_balance = data.credit_balance;
					$scope.showhistory = true;
					$scope.loader = false;
				}).error(function (data, status, headers, config) {
					
				});
			}
		}
	};
      
	document.removeEventListener("backbutton", onBackKeyDown, false);  
});

