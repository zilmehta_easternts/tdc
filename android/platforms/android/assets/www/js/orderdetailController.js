lsdApp.controller('orderdetailController', function($scope,$http,$route,$window,$rootScope,$location,$routeParams) 
{
	$scope.orderid = $routeParams.id;
	$scope.orderdetail = null;
	$rootScope.title("Order No " + $scope.orderid);
	
	if($rootScope.checkConnection())
	{
		$scope.loader = true;
		
		var dataObject = {
			order_id: $scope.orderid
		};
		
		var serdata = serialize(dataObject);
		
		console.log(serdata);
		
		$http({
			url: serviceurl +"orderdetail.php",
			method: "POST",
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			},
			data: serdata
		}).success(function (data, status, headers, config) {
			
			$scope.orderdetail = data;
			$scope.loader = false;
		}).error(function (data, status, headers, config) {
			
		});
	}
	document.removeEventListener("backbutton", onBackKeyDown, false); 
});
