lsdApp.controller('categoryController', function($scope,$rootScope,$location,$window,$http,$routeParams)
{
	$rootScope.header(false);
	$rootScope.title('');
	if($rootScope.checkConnection())
	{
		$scope.loader = true;
		$scope.categoryid = $routeParams.id;
		$scope.categoryname = '';
		$scope.otherbox = 0;
		$scope.other = '';
		$scope.qty = '1';
		$scope.info = {};
		$scope.specification = '';
		var dataObject = {
			categories_id : $scope.categoryid 
		};
		
		$scope.additionalamt = 0;	
		// serialize data before sending       
		var serdata = serialize(dataObject);
			
		$http({
		url: serviceurl + "products.php",
		method: "POST",
		headers: {'Content-Type': 'application/x-www-form-urlencoded'},
		data: serdata
		}).success(function(data, status, headers, config) 
		{	
			$rootScope.title(data.category_name);
			$scope.categoryname = data.category_name;
			$scope.details = data;
			$scope.products = data.products;
			$scope.products.unshift({ productid:'-1', productname:'Select Flavor', has_flatprice:'N',pricetype : [],productoptions : []});
			$scope.flatprc = 0;
			$scope.showdetails = false;
			if($scope.products.length != 0){
				 
				$scope.productname = data.products[0]; 
				/*
				$scope.prices = data.products[0].pricetype[0];
				$scope.totalprice = parseInt($scope.prices.price);
				if($scope.details.option_id != '') 	{
					$scope.alloptions = data.products[0].productoptions[0];
					data.products[0].productoptions.push({ value_id:'0', 'value_name':'Other' });
				}
				*/
			}
			
			if($scope.details.has_weight == 'Y') { $scope.weightoptions = 'kgs'; $scope.weight = 1; }
			$scope.loader = false;
			
		}).error(function (data, status, headers, config) {});
	}
	
	/*reset everything on product change */
	$scope.changeproductdetail = function()
	{	
		if($scope.productname.productid != '-1')
		{
			$scope.showdetails = true;
			console.log($scope.productname);
			$scope.flatprc = 0;
			if($scope.details.option_id != '') 	{
				$scope.alloptions = $scope.productname.productoptions[0];
				$scope.productname.productoptions.push({ value_id:'0', 'value_name':'Other' });
			}
			$scope.prices = $scope.productname.pricetype[0];
			$scope.totalprice = parseInt($scope.prices.price);
			document.getElementById("count").value = '1';
			/*
			var image = document.getElementById('myImage');
			image.src = '';
			image.style.display = 'none';
			*/
			if($scope.details.has_weight == 'Y') { $scope.weightoptions = 'kgs'; $scope.weight = 1; }
		}
		else
		{
			$scope.showdetails = false;
		}
	
	};
	
	/* Price type /Flat Price / weight change*/
	$scope.changeotherdetail = function()
	{
		if($scope.details.has_weight == 'Y') {
			if($scope.weight <= '500' && $scope.weightoptions == 'gms'){
				if($scope.flatprc == 0) { $scope.prices = $scope.productname.flatprice[0]; }
				$scope.flatprc = 1;	
				$scope.totalprice = parseInt($scope.prices.price);
			}else{	
				if($scope.flatprc == 1){ $scope.prices = $scope.productname.pricetype[0]; }
				if($scope.weightoptions == 'kgs'){	
					$scope.totalprice = parseInt($scope.prices.price * $scope.weight);
				}else{
					$scope.totalprice = parseInt($scope.prices.price * ($scope.weight / 1000));
				}
				$scope.flatprc = 0;
			}
		}else{
			$scope.flatprc = 0;	
			$scope.totalprice = $scope.prices.price;
		}
		console.log($scope.totalprice);
	};
	
	/* option change (shape)*/
	$scope.changeoption =  function()
	{
		console.log($scope.alloptions);
		if($scope.alloptions.value_id == 0 ){
			$scope.otherbox = 1;
		}else {
			$scope.otherbox = 0;
			$scope.other = '';
		}	
	};
	
	/* selected options */
	$scope.collectselections = function(){
		$scope.info['pid'] 		= $scope.productname.productid; // productid
		$scope.info['pname'] 	= $scope.productname.productname;//productname
		$scope.info['qty'] 		= document.getElementById("count").value; // qty
		$scope.info['cid'] 		= $scope.categoryid; // categoryid
		$scope.info['cname'] 	= $scope.categoryname; // categoryname
		$scope.info['imageurl'] = ''; // browse photo
		
		if($scope.details.has_weight == 'Y') {
			$scope.info['hasweight'] = 'Y';
			$scope.info['weight'] = $scope.weight + ' ' + $scope.weightoptions;
			if($scope.weight <= '500' && $scope.weightoptions == 'gms'){
				$scope.info['flatprice'] = 'Y';
				$scope.info['flatpriceid']    = $scope.prices.pricetype_id;
				$scope.info['flatpricename']  = $scope.prices.pricetype_name;
				$scope.info['fprice'] 		  = $scope.prices.price;
			}else{
				$scope.info['flatprice'] = 'N'; 
				$scope.info['pricetypeid'] 	  =  $scope.prices.pricetype_id;
				$scope.info['pricetypename']  = $scope.prices.pricetype_name;
				$scope.info['pprice'] 		  = $scope.prices.price;
			}
		}else{
			$scope.info['hasweight'] = 'N';
			$scope.info['weight'] 	 = '0';
			$scope.info['flatprice'] = 'N'; 
			$scope.info['pricetypeid'] 	  =  $scope.prices.pricetype_id;
			$scope.info['pricetypename']  = $scope.prices.pricetype_name;
			$scope.info['pprice'] 		  = $scope.prices.price;
		}
		
		$scope.info['amount'] 		 = $scope.info['qty'] * $scope.totalprice;
		$scope.info['additionalamt'] = $scope.additionalamt;
		$scope.info['specification'] = $scope.specification;
		$scope.info['totalprice'] 	 = parseInt($scope.info['amount']) + parseInt($scope.additionalamt) ;
		$scope.info['optionprice'] = 'N'; //'Y' for Regular Big Cupcake
		
		$scope.options = [];
	
		if($scope.details.option_id != '') 	{
			$scope.info['has_options'] = 'Y';
			
			if($scope.alloptions.value_id == 0){
				$scope.alloptions['other'] = $scope.other;
			}else{
				$scope.alloptions['other'] = '' ;
			}
			
			$scope.alloptions['qty'] = 0;
			$scope.options.push($scope.alloptions);
			
		}else{
			$scope.info['has_options'] = 'N' ;
			$scope.info['other'] = '' ;
		}
		
		$scope.info['optiondetails'] = $scope.options;
		$rootScope.addtocart($scope.info);
	};
	
	document.removeEventListener("backbutton", onBackKeyDown, false); 
});

