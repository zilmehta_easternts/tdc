lsdApp.controller('shoppingcartController', function($scope,$http,$route,$window,$rootScope,$location) {
    $scope.cart = [];
    $scope.cartoptions = [];
    $scope.detailform = false;
    $rootScope.title("Customer Cart");
    $scope.currentcatid = 0;
    var cartids = []
  	$scope.optns = [];
  	$scope.totalprice = 0;
  	$scope.finalamt = 0;
  	$scope.advance_amount = 0;
  	$scope.discount = 0;
  	$scope.left_amount = 0;
	db.transaction(function (tx)
	{
		tx.executeSql('SELECT * FROM customer_cart cc order by c_id asc', [], function (tx, results)
		{
			for(var i=0; i<results.rows.length;i++)
			{
				var cartprod = {};
				cartprod = results.rows.item(i);
				cartids.push(results.rows.item(i).id);
				console.log(cartprod.id);
				$scope.totalprice = parseFloat($scope.totalprice) + parseFloat(results.rows.item(i).total_price);
				$scope.$apply(function() {
					$scope.cart.push(cartprod);
				});
			}	
			 $scope.finalamt = $scope.totalprice;
		}, null);
		
	});
	
	
	db.transaction(function (tx1)
	{	
		for(i=0;i< cartids.length;i++)
		{
			console.log("SELECT * FROM customer_cart_options where cart_id = " + cartids[i]);
			tx1.executeSql('SELECT * FROM customer_cart_options where cart_id = ? ', [cartids[i]], function (tx1, results)
			{		
				var opt = [];
				if(results.rows.length > 0)
				{
					for(var j=0; j< results.rows.length; j++)
					{
						console.log(results.rows.item(j));
						$scope.info = {};
						$scope.info['cart_id'] = results.rows.item(j).cart_id;
						$scope.info['option_id'] = results.rows.item(j).option_id;
						$scope.info['option_name'] = results.rows.item(j).option_name;
						$scope.info['option_price'] = results.rows.item(j).option_price;
						$scope.info['other'] = results.rows.item(j).other;
						$scope.info['oqty'] = results.rows.item(j).oqty;
						opt.push($scope.info);	
					}	
				}	
				$scope.$apply(function() 
				{
						$scope.optns.push(opt);
				});
				
			}, null);
		}
		
	});
	
	$scope.updatecat = function(current) 
	{
		$scope.currentcatid = current;
	};
	
	$scope.confirmorder = function()
	{
		 $scope.detailform = true;
	};
	
	$scope.checkout = function()
	{
		// Put navigator.confirm
		var productsid = '';
		var quantities = '' ;
		var categoriesid = '' ;
		for(i=0;i< $scope.cart.length;i++)
		{
			console.log("i:" + $scope.optns[i]);
			
			if(productsid == '')
 			{
				productsid 		= $scope.cart[i].p_id ;		
 	  	 		quantities 		= $scope.cart[i].qty ;
 	  	 		categoriesid 	= $scope.cart[i].c_id ;
 	  	 		hasoptions 		= $scope.cart[i].has_options;
 	  	 		flatprice 		= $scope.cart[i].flat_price;
 	  	 		weight 			= $scope.cart[i].weight;
 	  	 		ptypeid 		= $scope.cart[i].ptype_id;
 	  	 		ptypename 		= $scope.cart[i].ptype_name;
 	  	 		pprice 			= $scope.cart[i].pprice;
 	  	 		ftypeid 		= $scope.cart[i].ftype_id;
 	  	 		ftypename 		= $scope.cart[i].ftype_name;
 	  	 		fprice 			= $scope.cart[i].fprice;
 	  	 		amt 			= $scope.cart[i].amount;
 	  	 		addamt 			= $scope.cart[i].add_amt;
 	  	 		totalamt 		= $scope.cart[i].total_price;
 	  	 		
 	  	 		
				newopt = '';
				for(k=0;k<$scope.optns[i].length;k++)
				{
						newopt = newopt + $scope.optns[i][k].option_id +"-" +$scope.optns[i][k].option_name + '-' + $scope.optns[i][k].option_price + '-' + $scope.optns[i][k].oqty +  '-' + $scope.optns[i][k].other + ",";
				}
				
				newopt = newopt.substring(0, newopt.length-1);
				
				
				totaloptions = newopt;
				
 	  	 		console.log("Opt:"+ totaloptions);
			}
			else
			{
				productsid 		= productsid + ',' + $scope.cart[i].p_id ;		
 	  	 		quantities 		= quantities + ',' + $scope.cart[i].qty ;
 	  	 		categoriesid 	= categoriesid + ',' + $scope.cart[i].c_id ;
 	  	 		hasoptions 		= hasoptions + ',' + $scope.cart[i].has_options;
 	  	 		flatprice 		= flatprice + ',' + $scope.cart[i].flat_price;
 	  	 		weight 			= weight + ',' + $scope.cart[i].weight;
 	  	 		ptypeid 		= ptypeid + ',' + $scope.cart[i].ptype_id;
 	  	 		ptypename 		= ptypename + ',' + $scope.cart[i].ptype_name;
 	  	 		pprice 			= pprice + ',' + $scope.cart[i].pprice;
 	  	 		ftypeid 		= ftypeid  + ','+ $scope.cart[i].ftype_id;
 	  	 		ftypename 		= ftypename  + ','+ $scope.cart[i].ftype_name;
 	  	 		fprice 			= fprice  + ','+ $scope.cart[i].fprice;
 	  	 		amt 			= amt + ',' + $scope.cart[i].amount;
 	  	 		addamt 			= addamt + ',' + $scope.cart[i].add_amt;
 	  	 		totalamt 		= totalamt + ',' + $scope.cart[i].total_price;
 	  	 		
 	  	 		
				newopt = '';
				for(k=0;k<$scope.optns[i].length;k++)
				{
						newopt = newopt + $scope.optns[i][k].option_id +"-" +$scope.optns[i][k].option_name + '-' + $scope.optns[i][k].option_price + '-' + $scope.optns[i][k].oqty +  '-' + $scope.optns[i][k].other + ",";
				}
				
				newopt = newopt.substring(0, newopt.length-1);
				
				totaloptions = totaloptions + ':' + newopt;
				console.log("Opt1:"+ totaloptions);
			}
		}
		
		var dataObject = 
		{
	          customer_id 	: '1',
	          deliverydate 	: '',
	          deliverytime 	: '',
	          deliveryadd 	: '',
	          products 		: productsid,
	          pquantities 	: quantities , 
	          pcategoriesid	: categoriesid , 
	          hasoptions 	: hasoptions,
	          totaloptions 	: totaloptions,
	          flatprice 	: flatprice,
	          weight    	: weight,
	          ptypeid 		: ptypeid,
			  ptypename     : ptypename,
			  pprice 		: pprice,
			  ftypeid 		: ftypeid,
			  ftypename 	: ftypename,
			  fprice 		: fprice,
			  amt 			: amt,
			  addamt		: addamt,
			  totalamt		: totalamt,
			  subtotal		: $scope.totalprice,
			  discount		: $scope.discount,
			  total			: $scope.finalamt,
			  advanceamt	: $scope.advance_amount,
			  leftamt		: $scope.left_amount,
			  creditused	: '0',
			  creditid		: '',
			  creditamt		: ''
	    };
	       
	   console.log(dataObject);
	   var serdata = serialize(dataObject);
	   console.log(serdata);
	   
	    $http({
	    	 url: serviceurl + "checkout.php",
	         method: "POST",
	         headers: {'Content-Type': 'application/x-www-form-urlencoded'},
	         data: serdata
	    	}).success(function(data, status, headers, config) {
	    		console.log(data);
			if(data.msg == 1)
	    	{
	    		alert('Order has been successfully placed. : Order ID - ' + data.orderid);
			//	$rootScope.calculatecnt();
			
	    	}
	    	else 
	    	{
	    		$scope.message = "There occured some problem in placing the order..Please try agin later. "
	    	}
	    	}).error(function(data, status, headers, config) {
	    			
		});
	}
	
	
            
});
