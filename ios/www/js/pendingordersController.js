lsdApp.controller('pendingordersController', function($scope,$http,$route,$window,$rootScope,$location) 
{
	
	$rootScope.title("Pending Orders");
	
	$scope.orders = '';
	/* Date Section */
	$scope.today = function () 
	{
		$scope.start = new Date();
	};
	
	$scope.today();

	$scope.clear = function () {
		$scope.start = null;
	};
	
	$scope.open = function ($event) {
		$event.preventDefault();
		$event.stopPropagation();
		$scope.opened = !$scope.opened; // alter date menu
	};

	$scope.dateOptions = {
		formatYear: 'yy',
		startingDay: 1
	};
	
	/* End of Date */
	if($rootScope.checkConnection())
	{
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth()+1; //January is 0!
            var yyyy = today.getFullYear();
                  
            if(dd<10) {
                  dd='0'+dd;
            }
                  
            if(mm<10) {
                  mm='0'+mm;
            }
                  
            today = dd+'-'+mm+'-'+yyyy;
                  
		$scope.loader = true;
		
		
		var dataObject = {
			status: 'pending',
			order_date : today
		};
		
        
		var serdata = serialize(dataObject);
		console.log(serdata);
		
		$http({
			url: serviceurl +"orders.php",
			method: "POST",
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded'
			},
			data: serdata
		}).success(function (data, status, headers, config) {
			console.log(data);
			$scope.orders = data;
			$scope.loader = false;
		}).error(function (data, status, headers, config) {
			
		});
		
	}
	
	$scope.submitTheForm = function()
	{
		if($rootScope.checkConnection())
		{
			$scope.loader = true;
			var dataObject = {
			status: 'pending',
			order_date : document.getElementById('start').value
			};
			
			var serdata = serialize(dataObject);
			console.log(serdata);
			
			$http({
				url: serviceurl +"orders.php",
				method: "POST",
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded'
				},
				data: serdata
			}).success(function (data, status, headers, config) {
				console.log(data);
				$scope.orders = data;
				$scope.loader = false;
			}).error(function (data, status, headers, config) {
				
			});
		}
	};
	document.removeEventListener("backbutton", onBackKeyDown, false); 
});
