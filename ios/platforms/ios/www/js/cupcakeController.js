lsdApp.controller('cupcakecategoryController', function($scope,$rootScope,$location,$window,$http,$routeParams)
{
	$rootScope.header(false);
	$rootScope.title('');
	if($rootScope.checkConnection())
	{
		$scope.loader = true;
		$scope.categoryid = $routeParams.id;
		$scope.totalprice = 0;
		$scope.total = 0;
        $scope.totalqty = 1;
		$scope.$watchCollection(['totalprice','total','totalqty'], function (newvalue, oldvalue, $scope) {
			
		});
		$scope.specification = '';
		
		var dataObject = 
		{
			categories_id : $scope.categoryid 
		};
		
		$scope.additionalamt = 0;	

		var serdata = serialize(dataObject);
			
		$http({
		url: serviceurl + "products.php",
		method: "POST",
		headers: {'Content-Type': 'application/x-www-form-urlencoded'},
		data: serdata
		}).success(function(data, status, headers, config) 
		{	
			$rootScope.title(data.category_name);	
			$scope.categoryname = data.category_name;
			$scope.details = data;
			$scope.products = data.products;
			$scope.flavors = data.products[0].productoptions;
			if($scope.products.length != 0) 	 
			{ 
				$scope.productname = data.products[0]; 
				$scope.prices = data.products[0].pricetype[0];	
				$scope.totalprice =  parseInt(data.products[0].pricetype[0].price);	
				
			}
			$scope.loader = false;
		}).error(function (data, status, headers, config) {});
	}
	
	
	$scope.changeproductdetail = function()
	{	
		var selectedproduct = $scope.productname;
		
		$scope.flavors = selectedproduct.productoptions;
		
		$scope.prices = selectedproduct.pricetype[0];	
		$scope.totalprice =  parseInt(selectedproduct.pricetype[0].price);
		 $scope.selected = [] ;
		 $scope.array = [];
		$scope.array_ = angular.copy($scope.array);
		
		/*
		var image = document.getElementById('myImage');
		image.src = '';
		image.style.display = 'none';
		*/
        $scope.totalprice = $scope.totalprice * $scope.totalqty;
	};
	
	$scope.changepricetype = function()
	{
		var selectedproduct = $scope.productname;
		if($scope.productname.productname == 'Big' && $scope.prices.pricetype_name == 'Regular')
		  {
			  $scope.total = 0;
			  if($scope.selected.length == 0)
			  {
				 $scope.total = 0;
				 $scope.totalprice = $scope.total;
			  }
			 for(var i= 0;i < $scope.flavors.length; i++)
			 {	
				for(j=0;j < $scope.selected.length; j++)
				{
					if($scope.selected[j] == $scope.flavors[i].value_id)
					{
						$scope.total = parseInt($scope.total) +  (parseInt(document.getElementById("count_"+$scope.selected[j]).value) * parseInt($scope.flavors[i].price));
						$scope.totalprice = $scope.total;
						//alert($scope.totalprice);
						console.log($scope.totalprice);
						break;
					}
				}			
			 }
           }else if($scope.productname.productname == 'Big' && $scope.prices.pricetype_name == 'Customised1'){
                  
                  $scope.total = 0;
                  if($scope.selected.length == 0)
                  {
                    $scope.total = 0;
                    $scope.totalprice = $scope.total;
                  }
                  for(var i= 0;i < $scope.flavors.length; i++)
                  {
                    for(j=0;j < $scope.selected.length; j++)
                    {
                        if($scope.selected[j] == $scope.flavors[i].value_id)
                        {
                            $scope.total = parseInt($scope.total) +  (parseInt(document.getElementById("count_"+$scope.selected[j]).value) * parseInt($scope.prices.price));
                            $scope.totalprice = $scope.total;
                            //alert($scope.totalprice);
                            console.log($scope.totalprice);
                            break;
                        }
                    }
                  }	 

          }else{
			$scope.totalprice = parseInt($scope.prices.price);	
		 }
                  
        $scope.totalprice = $scope.totalprice * $scope.totalqty;
	};
	
	/* Beginning Of check Boxes Code */
	/* selection of checkboxes */
     $scope.selected = []; 
      var updateSelected = function(action, id) {
		  if (action === 'add' && $scope.selected.indexOf(id) === -1) {
			$scope.selected.push(id);
		  }
		  if (action === 'remove' && $scope.selected.indexOf(id) !== -1) {
			$scope.selected.splice($scope.selected.indexOf(id), 1);
		  }
  
		  if($scope.productname.productname == 'Big' && $scope.prices.pricetype_name == 'Regular')
		  {
			  $scope.total = 0;
			  if($scope.selected.length == 0)
			  {
				 $scope.total = 0;
				 $scope.totalprice = $scope.total;
			  }
			 for(var i= 0;i < $scope.flavors.length; i++)
			 {	
				for(j=0;j < $scope.selected.length; j++)
				{
					if($scope.selected[j] == $scope.flavors[i].value_id)
					{
						$scope.total = parseInt($scope.total) +  (parseInt(document.getElementById("count_"+$scope.selected[j]).value) * parseInt($scope.flavors[i].price));
						$scope.totalprice = $scope.total;
						//alert($scope.totalprice);
						console.log($scope.totalprice);
						break;
					}
				}			
			  }
            $scope.totalprice = $scope.totalprice * $scope.totalqty;
          }else if($scope.productname.productname == 'Big' && $scope.prices.pricetype_name == 'Customised1'){
                  $scope.total = 0;
                  if($scope.selected.length == 0)
                  {
                    $scope.total = 0;
                    $scope.totalprice = $scope.total;
                  }
                  for(var i= 0;i < $scope.flavors.length; i++)
                  {
                    for(j=0;j < $scope.selected.length; j++)
                    {
                        if($scope.selected[j] == $scope.flavors[i].value_id)
                        {
                            $scope.total = parseInt($scope.total) +  (parseInt(document.getElementById("count_"+$scope.selected[j]).value) * parseInt($scope.prices.price));
                            $scope.totalprice = $scope.total;
                            //alert($scope.totalprice);
                            console.log($scope.totalprice);
                            break;
                        }
                    }
                  }
            $scope.totalprice = $scope.totalprice * $scope.totalqty;
         }else{}
	};

	$scope.updateSelection = function($event, id) {
	  var checkbox = $event.target;
	  var action = (checkbox.checked ? 'add' : 'remove');
	  updateSelected(action, id);
	};

	$scope.selectAll = function($event) {
	  var checkbox = $event.target;
	  var action = (checkbox.checked ? 'add' : 'remove');
	  for ( var i = 0; i < $scope.entities.length; i++) {
		var entity = $scope.entities[i];
		updateSelected(action, entity.id);
	  }
	};

	$scope.getSelectedClass = function(entity) {
	  return $scope.isSelected(entity.value_id) ? 'selected' : '';
	};

	$scope.isSelected = function(id) {
	  return $scope.selected.indexOf(id) >= 0;
	};

	$scope.isSelectedAll = function() {
	  return $scope.selected.length === $scope.entities.length;
	};
	
	/* End Of Checkboxes Code*/
	
	$scope.incdec1=function(plusminus,option_id)
	{
		plusminus =parseInt(plusminus);	
		currentlife= parseInt(document.getElementById("count_"+option_id).value) + plusminus;
		//alert(currentlife);
		//document.getElementById("count_"+option_id).value =currentlife;
		if(currentlife<=1)
		{
			currentlife=1;
		}
		document.getElementById("count_"+option_id).value =currentlife;
		
		if($scope.productname.productname == 'Big' && $scope.prices.pricetype_name == 'Regular')
		{
			  $scope.total = 0;
			  if($scope.selected.length == 0)
			  {
				 $scope.total = 0;
				 $scope.totalprice = $scope.total;
			  }
			 for(var i= 0;i < $scope.flavors.length; i++)
			 {	
				for(j=0;j < $scope.selected.length; j++)
				{
					if($scope.selected[j] == $scope.flavors[i].value_id)
					{
						$scope.total = parseInt($scope.total) +  (parseInt(document.getElementById("count_"+$scope.selected[j]).value) * parseInt($scope.flavors[i].price));
						$scope.totalprice = $scope.total;
						//alert($scope.totalprice);
						console.log($scope.totalprice);
						break;
					}
				}			
			}
                  $scope.totalprice = $scope.totalprice * $scope.totalqty;
                  }else if($scope.productname.productname == 'Big' && $scope.prices.pricetype_name == 'Customised1'){
                  $scope.total = 0;
                  if($scope.selected.length == 0)
                  {
                  $scope.total = 0;
                  $scope.totalprice = $scope.total;
                  }
                  for(var i= 0;i < $scope.flavors.length; i++)
                  {
                  for(j=0;j < $scope.selected.length; j++)
                  {
                  if($scope.selected[j] == $scope.flavors[i].value_id)
                  {
                  $scope.total = parseInt($scope.total) +  (parseInt(document.getElementById("count_"+$scope.selected[j]).value) * parseInt($scope.prices.price));
                  $scope.totalprice = $scope.total;
                  //alert($scope.totalprice);
                  console.log($scope.totalprice);
                  break;
                  }
                  }
                  }
                  $scope.totalprice = $scope.totalprice * $scope.totalqty;
                  }else{}
	};
	
	/* selected options */
	$scope.collectselections = function(){
	$scope.info = {};
	
		$scope.info['pid'] 		= $scope.productname.productid; // productid
		$scope.info['pname'] 	= $scope.productname.productname;//productname
		$scope.info['qty'] 		= $scope.totalqty; // qty
		$scope.info['cid'] 		= $scope.categoryid; // categoryid
		$scope.info['cname'] 	= $scope.categoryname; // categoryname
		$scope.info['imageurl'] = ''; // browse photo
		
		$scope.info['hasweight'] = 'N';
		$scope.info['weight'] 	 = '0';
		$scope.info['flatprice'] = 'N'; 
		$scope.info['pricetypeid'] 	  =  $scope.prices.pricetype_id;
		$scope.info['pricetypename']  = $scope.prices.pricetype_name;
		$scope.info['pprice'] 		  = $scope.prices.price;
		
		
		$scope.info['amount'] 		 = $scope.totalprice;
		$scope.info['additionalamt'] = $scope.additionalamt;
		$scope.info['totalprice'] 	= parseInt($scope.info['amount']) + parseInt($scope.additionalamt) ;
		$scope.info['specification'] = $scope.specification;
		
		if($scope.productname.productname == 'Big' && $scope.prices.pricetype_name == 'Regular')
		{
			$scope.info['option_price'] = 'Y'; //'Y' for Regular Big Cupcake
		}
		else
		{
			$scope.info['option_price'] = 'N'; 
		}
		
		$scope.selectedflavors = [];
		$scope.alloptions = {};
		
		$scope.info['has_options'] = 'Y';
		
		
		for(var i= 0;i < $scope.flavors.length; i++)
		 {	
			for(j=0;j < $scope.selected.length; j++)
			{	
				if($scope.selected[j] == $scope.flavors[i].value_id)
				{
					$scope.flavors[i]['qty'] = document.getElementById("count_"+$scope.selected[j]).value;
					$scope.flavors[i]['other'] = '';
					if($scope.productname.productname == 'Big' && $scope.prices.pricetype_name == 'Regular')
					{
						
						$scope.flavors[i]['price'] = $scope.flavors[i].price;
					}
					
					$scope.selectedflavors.push($scope.flavors[i]);
					console.log($scope.flavors[i]);
					break;
					
				}
			}			
		}	
		
		$scope.info['optiondetails'] = $scope.selectedflavors;
		
		console.log($scope.info);
	
	
		$rootScope.addtocart($scope.info);
		
	};
                  
    $scope.incdecqty=function(plusminus)
    {
            plusminus =parseInt(plusminus);
            currentlife= parseInt($scope.totalqty) + plusminus;
                  
            $scope.totalqty =currentlife;
            if(currentlife<=1)
            {
                  currentlife=1;
            }
            $scope.totalqty.value =currentlife;
                  
            $scope.changepricetype();
    };
	
	document.removeEventListener("backbutton", onBackKeyDown, false); 
});

