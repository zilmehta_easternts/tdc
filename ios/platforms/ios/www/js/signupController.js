lsdApp.controller('signupController', function($scope,$rootScope,$location,$window,$http,$route)
{
		$rootScope.header(true);
		$rootScope.title("Signup");
		
		$scope.formInfo = {};
		
		$scope.submitTheForm = function(item, event) 
		{
			if($rootScope.checkConnection())
			{	
				$scope.loader = true;
				var serdata = serialize($scope.formInfo);
				
				$http({
				url: serviceurl+"signup.php",
				method: "POST",
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				data: serdata
				}).success(function(data, status, headers, config) {
					if(data.status > 1){
						/*alert('You are signed up successfully');*/
                        navigator.notification.alert('You are signed up successfully!', function(){}, "Success", "");
                        $location.path("/login") ;
					}else if(data.status == -1){
                        navigator.notification.alert('Username Already Exists. Please Try another username!', function(){}, "Failure", "");
                       // alert('Username Already Exists. Please Try another username!');
                        $scope.formInfo.username = '';
                        $scope.formInfo.password = '';
                        $scope.loader = false;
                    }else{	
						navigator.notification.alert('Invalid Data', function(){}, "Failure", "");
                        $scope.loader = false;
					}			  	
				}).error(function(data, status, headers, config) {	
					//navigator.notification.alert('Submitting form failed!', function(){}, "Failure", "");	
				});	   
			}	
        }
        
        $scope.goToLogin = function(){
            $location.path("/login") ;
        }
		
	  document.addEventListener("backbutton", onBackKeyDown, false); 
});
