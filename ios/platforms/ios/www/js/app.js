var lsdApp = angular.module('lsdApp', ["ngRoute", "ngStorage", "ngSanitize", "MassAutoComplete", "mobile-angular-ui", "ui.bootstrap"]);

var serviceurl = "http://thedoughcompany.in/data2/";

var db;
/* to display special characters*/
lsdApp.filter('html', function ($sce) {
    return function (input) {
        return $sce.trustAsHtml(input);
    }
})

function removeHTMLTags() {
    var strInputCode = document.getElementById("overviewid").innerHTML;
    strInputCode = strInputCode.replace(/&(lt|gt);/g, function (strMatch, p1) {
        return (p1 == "lt") ? "<" : ">";
    });
    var strTagStrippedText = strInputCode.replace(/<\/?[^>]+(>|$)/g, "");
    return strTagStrippedText;
}

/* serailiaze data for sending to server */
function serialize(obj, prefix) {
    var str = [];
    for (var p in obj) {
        if (obj.hasOwnProperty(p)) {
            var k = prefix ? prefix + "[" + p + "]" : p,
                v = obj[p];
            str.push(typeof v == "object" ? serialize(v, k) : encodeURIComponent(k) + "=" + encodeURIComponent(v));
        }
    }
    return str.join("&");
}

function onBackKeyDown() {
    navigator.app.exitApp();
}

function onDeviceReady() {
    db = openDatabase('mydb', '1.0', 'Test DB', 2 * 1024 * 1024);
    db.transaction(populateDB, errorCB, successCB);
}


function openphoto() {
    navigator.camera.getPicture(onSuccess, onFail, {
        quality: 10,
        destinationType: Camera.DestinationType.FILE_URI,
        sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
        correctOrientation: true
    });

}


function onSuccess(imageURI) {
    var image = document.getElementById('myImage');
    image.src = imageURI;
    image.style.display = 'block';

}

function onFail(message) {
    //alert('Failed because: ' + message);
}

document.addEventListener('deviceready', onDeviceReady, false);


/* create tables if not exists */
function populateDB(tx, res) {
    // tx.executeSql('DROP TABLE IF EXISTS customer_cart');
    //tx.executeSql('DROP TABLE IF EXISTS customer_cart_options');

    tx.executeSql('CREATE TABLE IF NOT EXISTS customer_cart (id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,customer_id INTEGER,p_id INTEGER,p_name VARCHAR(100),c_id INTEGER,c_name  VARCHAR(100),image_url TEXT ,qty INTEGER,has_options char(2) , option_price char(2) ,flat_price char(2), has_weight char(2) , weight TEXT , ptype_id INTEGER,ptype_name VARCHAR(50), pprice VARCHAR(20), ftype_id INTEGER,ftype_name VARCHAR(50) ,fprice VARCHAR(20), amount TEXT , add_amt TEXT,total_price TEXT,specification TEXT)');

    tx.executeSql('CREATE TABLE IF NOT EXISTS customer_cart_options (id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,cart_id INTEGER , option_id INTEGER , option_name TEXT, option_price TEXT,other TEXT,oqty INTEGER)');

    /* cart count */

    tx.executeSql('SELECT * FROM customer_cart', [], function (tx, results) {
        var len = results.rows.length;

    }, null);
}

function errorCB(err) {
    console.log("Error processing SQL: " + err.code);
}

function successCB() {
    console.log("Successful!");

}

var win = function (r) {
    console.log("Code = " + r.responseCode);
    console.log("Response = " + r.response);
    console.log("Sent = " + r.bytesSent);
}

var fail = function (error) {
    console.log("An error has occurred: Code = " + error.code);
    console.log("upload error source " + error.source);
    console.log("upload error target " + error.target);
}

lsdApp.directive("ngMobileClick", [function () {
    return function (scope, elem, attrs) {
        elem.bind("touchend click", function (e) {
            e.preventDefault();
            e.stopPropagation();
            scope.$apply(attrs["ngMobileClick"]);
        });
    }
}])


lsdApp.config(function ($routeProvider) {
    $routeProvider
    // route for the home page
        .when('/home', {
            templateUrl: 'partials/home.html',
            controller: 'mainController'
        })
        .when('/category/:id', {
            templateUrl: 'partials/category.html',
            controller: 'categoryController'
        })
        .when('/cupcakecategory/:id', {
            templateUrl: 'partials/cupcakecategory.html',
            controller: 'cupcakecategoryController'
        })
        .when('/allcategories', {
            templateUrl: 'partials/allcategories.html',
            controller: 'allcategoriesController'
        })
        .when('/product/:id', {
            templateUrl: 'partials/productdetail.html',
            controller: 'productdetailController'
        })
        .when('/login', {
            templateUrl: 'partials/login.html',
            controller: 'loginController'
        })
        .when('/logout', {
            templateUrl: 'partials/logout.html',
            controller: 'logoutController'
        })
        .when('/shoppingcart', {
            templateUrl: 'partials/shoppingcart.html',
            controller: 'shoppingcartController'
        })
        .when('/pendingorders', {
            templateUrl: 'partials/pendingorders.html',
            controller: 'pendingordersController',
            label: 'Orders'
        })
        .when('/pendingorderdetail/:id', {
            templateUrl: 'partials/pendingorderdetail.html',
            controller: 'pendingorderdetailController',
            label: 'Orders'
        })
        .when('/orders/:type', {
            templateUrl: 'partials/orders.html',
            controller: 'ordersController',
            label: 'Orders'
        })
        .when('/orderdetail/:id', {
            templateUrl: 'partials/orderdetail.html',
            controller: 'orderdetailController',
            label: 'Orders'
        })
        .when('/creditnote', {
            templateUrl: 'partials/creditnote.html',
            controller: 'creditnoteController',
            label: 'Add Credit Note'
        })
        .when('/viewcredit', {
            templateUrl: 'partials/viewcredits.html',
            controller: 'viewcreditController',
            label: 'View Credit Note'
        })
        .when('/allorders', {
            templateUrl: 'partials/allorders.html',
            controller: 'allordersController',
            label: 'Orders'
        })
        .when('/categoryorder/:id/:name', {
            templateUrl: 'partials/categoryorder.html',
            controller: 'categoryorderController',
            label: 'View Credit Note'
        })
        .when('/signup', {
            templateUrl: 'partials/signup.html',
            controller: 'signupController'
        })
        .otherwise({
            templateUrl: 'partials/login.html',
            controller: 'loginController'
        });

});

/* called on every route*/

lsdApp.run(function ($rootScope, $localStorage, $http, $route, $location) {
    //document.getElementById('logo-title').innerHTML = 'TDC' ;

    $rootScope.$storage = $localStorage.$default({
        login1: 0,
        customer_id: 0,
        customer_name: '',
        credit_amount: '0',
        has_credit: 'N',
        order_deliverydate: '',
        username: '',
        orders: '',
        allcustomers: ''
    });

    /* set title for all pages */
    $rootScope.title = function (title) {
        document.getElementById('brand-title').innerHTML = title;
    };
    /* set header for all pages */
    $rootScope.header = function (common) {
        $rootScope.defaultheader = common;
    };

    /* set footer for all pages */
    $rootScope.footer = function (common) {
        $rootScope.innerfooter = common;

    };

    $rootScope.refresh = function () {
        $route.reload();
    };

    $http.get(serviceurl + 'allcategories.php')
        .success(function (data) {
            $rootScope.allcategories = data.Categories;
            console.log($rootScope.allcategories);
        })
        .error(function (data, status, headers, config) {
        });

    $rootScope.incdec = function (plusminus) {
        plusminus = parseInt(plusminus);
        currentlife = parseInt(document.getElementById("count").value) + plusminus;

        //document.getElementById("count").value =currentlife;
        if (currentlife <= 1) {
            currentlife = 1;
        }
        document.getElementById("count").value = currentlife;
    };

    $rootScope.downloadcustomers = function () {
        if ($rootScope.checkConnection()) {
            navigator.notification.confirm(
                'Do You want to download Customer List ?', // message
                function (button) {
                    if (button == 1) {
                        $rootScope.$apply(function () {
                            $rootScope.loader = true;
                        });


                        $http.get(serviceurl + 'customers.php')
                            .success(function (data) {
                                //$rootScope.loader = false;
                                $rootScope.$storage.allcustomers = data.Customers;
                                $route.reload();

                            })
                            .error(function (data, status, headers, config) {
                            });

                    }
                },
                'Download Customers',           // title
                ['Yes', 'No']         // buttonLabels
            );
        }

    };

    $rootScope.addtocart = function (details) {
        if ($rootScope.$storage.customer_id == '0') {
            //alert('Please Select a Customer');
            navigator.notification.alert('Please Select a Customer', function () {
            }, "Error", "");
            $location.path('/home');
            return;
        }

        db.transaction(function (tx) {
            if (details.flatprice == 'Y') {
                var ptypeid = '0';
                var ptypename = '';
                var ptypeprice = '0';
                var ftypeid = details.flatpriceid;
                var ftypename = details.flatpricename;
                var ftypeprice = details.fprice;
            }
            else {
                var ptypeid = details.pricetypeid;
                var ptypename = details.pricetypename;
                var ptypeprice = details.pprice;
                var ftypeid = '0';
                var ftypename = '';
                var ftypeprice = '0';
            }

            var image = document.getElementById('myImage').src;

            tx.executeSql('INSERT INTO customer_cart (customer_id,p_id,p_name ,c_id ,c_name ,image_url ,qty ,has_options,option_price,flat_price,has_weight,weight,ptype_id,ptype_name,pprice,ftype_id,ftype_name,fprice,amount,add_amt,total_price,specification) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', ['1', details.pid, details.pname, details.cid, details.cname, image, details.qty, details.has_options, details.optionprice, details.flatprice, details.hasweight, details.weight, ptypeid, ptypename, ptypeprice, ftypeid, ftypename, ftypeprice, details.amount, details.additionalamt, details.totalprice, details.specification], querySuccess);

        });


        function querySuccess(tx, results) {
            console.log("Last inserted row ID = " + results.insertId);
            console.log(details.optiondetails);

            tx.executeSql('SELECT * FROM customer_cart', [], function (tx, results) {
                var len = results.rows.length;
                $rootScope.$apply(function () {
                    $rootScope.cartcnt = len;
                });
            }, null);

            if (details.has_options == 'Y') {
                for (i = 0; i < details.optiondetails.length; i++) {
                    if (details.option_price == 'Y') {
                        price = details.optiondetails[i].price;
                    } else {
                        price = 0;
                    }

                    tx.executeSql('INSERT INTO customer_cart_options (cart_id,option_id,option_name,option_price,other,oqty) VALUES (?,?,?,?,?,?)', [results.insertId, details.optiondetails[i].value_id, details.optiondetails[i].value_name, price, details.optiondetails[i].other, details.optiondetails[i].qty]);
                }
            }

        }

        $rootScope.$watch('cartcnt', function (newvalue, oldvalue, $scope) {
            $rootScope.cartcnt = newvalue;
        });

        navigator.notification.alert('Successfully Added To Cart!', function () {
        }, "Success", "");

        setTimeout(function () {
            $route.reload();
        }, 1000);

    };

    $rootScope.removefromcart = function (cartid) {
        navigator.notification.confirm(
            'Do You want to remove this item from Cart ?', // message
            function (button) {
                if (button == 1) {
                    db.transaction(function (tx) {
                        tx.executeSql('DELETE FROM customer_cart WHERE id = ?', [cartid]);
                    });

                    db.transaction(function (tx) {
                        tx.executeSql('DELETE FROM customer_cart_options WHERE cart_id = ?', [cartid]);
                    });

                    $rootScope.calculatecnt();
                    $route.reload();
                }
            },
            'Remove from Cart',           // title
            ['Yes', 'No']         // buttonLabels
        );

    }

    $rootScope.calculatecnt = function () {
        db.transaction(function (tx) {
            tx.executeSql('SELECT * FROM customer_cart', [], function (tx, results) {
                var len = results.rows.length;
                $rootScope.$apply(function () {
                    $rootScope.cartcnt = len;
                });


            }, null);
        });

    };

    $rootScope.$watch('customername', function (newvalue, oldvalue, $scope) {
        $rootScope.customername = newvalue;
    });

    $rootScope.resetcart = function () {
        $rootScope.$storage.customer_id = 0;
        $rootScope.$storage.customer_name = '';
        $rootScope.$storage.credit_amount = '0';
        $rootScope.$storage.has_credit = 'N';
        $rootScope.$storage.order_deliverydate = '';
        $rootScope.deliverydate = '';
        $rootScope.orders = '';

        db.transaction(function (tx) {
            tx.executeSql('DELETE FROM customer_cart', []);
        });

        db.transaction(function (tx) {
            tx.executeSql('DELETE FROM customer_cart_options', []);
        });

        $rootScope.calculatecnt();
        $route.reload();
    };

    /* check for internet connection  */

    $rootScope.checkConnection = function () {

        // return true;
        if (navigator.network.connection.type == Connection.NONE) {
            navigator.notification.alert('Please check your Internet Connection', function () {
            }, "Warning", "");
            return false;
        }
        else {
            return true;
        }
    };

    /* end of connection check */
    $rootScope.go = function (pathname) {
        $location.path(pathname);
    }

    $rootScope.callfn = function () {
        openphoto();
    }


});


lsdApp.controller('mainController', function ($scope, $rootScope, $location, $window, $http, $sce, $q, $modal, $log) {

    $rootScope.customername = '';
    $rootScope.title('Home');
    $rootScope.header(false);
    $scope.customers = null;

    var options = {
        date: new Date(),
        mode: 'date', // or 'time'
        allowOldDates: true,
        allowFutureDates: true,
        doneButtonLabel: 'DONE',
        doneButtonColor: '#F2F3F4',
        cancelButtonLabel: 'CANCEL',
        cancelButtonColor: '#000000'
    };

    $scope.ondateSuccess = function (date) {
        //alert('Selected date: ' + date);
        document.getElementById('start').value = date;
    }


    $scope.demo = function ($event) {

        datePicker.show(options, $scope.ondateSuccess);
    }


    if ($rootScope.$storage.allcustomers.length == 0) {
        $rootScope.loader = true;
        $http.get(serviceurl + 'customers.php')
            .success(function (data) {
                $scope.customers = data.Customers;
                $rootScope.$storage.allcustomers = data.Customers;
                $rootScope.loader = false;
            })
            .error(function (data, status, headers, config) {
            });

    } else {
        $scope.customers = $rootScope.$storage.allcustomers;
    }

    /* auto search code */
    $scope.dirty = {};

    function highlight(str, term) {
        var highlight_regex = new RegExp('(' + term + ')', 'gi');
        return str.replace(highlight_regex,
            '<span class="highlight">$1</span>');
    };

    function suggest_users(term) {
        if (term.length < 4) {
            return;
        }

        var q = term.toLowerCase().trim(),
            results = [];
        if ($scope.customers != null) {
            for (var i = 0; i < $scope.customers.length; i++) {
                var customer = $scope.customers[i];
                if (customer.customers_name.toLowerCase().indexOf(q) !== -1 ||
                    customer.customers_mobile.toLowerCase().indexOf(q) !== -1)
                    results.push({
                        value: customer.customers_mobile,
                        // Pass the object as well. Can be any property name.
                        obj: customer,
                        label: $sce.trustAsHtml(
                            '<div class="row nomargin">' +
                            ' <div class="col-xs-12">' +
                            '  <div class="pull-right" style="padding : 0px 15px;"><i class="fa fa-user">&nbsp;&nbsp;</i> <strong>' + highlight(customer.customers_name, term) + '</strong></div>' +
                            '  <i class="fa fa-phone">&nbsp;&nbsp;</i><strong>' + highlight(customer.customers_mobile, term) + '</strong> ' +
                            ' </div>' +
                            '</div>'
                        )
                    });
            }
        }

        return results;
    };

    $scope.ac_options_users = {
        suggest: suggest_users,
        on_select: function (selected) {
            $scope.selected_customer = selected.obj;
            $rootScope.customername = $scope.selected_customer.customers_name;
            $rootScope.customerid = $scope.selected_customer.customers_id;
            $rootScope.customercredit = $scope.selected_customer.has_credit;
            $rootScope.creditamount = $scope.selected_customer.credit_amount;
            console.log($scope.selected_customer);
            document.activeElement.blur(); // close keypad
        }
    };
    /* end of auto search code*/

    /* modal box */


    $scope.animationsEnabled = true;

    $scope.open = function () {
        if ($rootScope.checkConnection()) {
            $scope.dirty.value = '';
            var modalInstance = $modal.open({
                animation: $scope.animationsEnabled,
                templateUrl: 'myModalContent.html',
                controller: 'ModalInstanceCtrl',
                size: 'sm',
                resolve: {
                    customers: function () {
                        return $scope.customers;
                    }
                }
            });
        }

    };


    /* end of modal box */

    $scope.proceed = function () {
        $rootScope.$storage.customer_id = $rootScope.customerid;
        $rootScope.$storage.customer_name = $rootScope.customername;
        $rootScope.$storage.has_credit = $rootScope.customercredit;
        $rootScope.$storage.credit_amount = $rootScope.creditamount;
        if ($rootScope.deliverydate == '' || typeof($rootScope.deliverydate) == 'undefined') {
            $rootScope.$storage.order_deliverydate = document.getElementById('start').value;
        }
        else {
            $rootScope.$storage.order_deliverydate = $rootScope.deliverydate;

        }

        $location.path('/allcategories');
    };

    /* Date Section */
    $scope.today = function () {
        $scope.delivery_date = new Date();
    };

    $scope.today();

    $scope.clear = function () {
        $scope.delivery_date = null;
    };

    $scope.dateopen = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.opened = !$scope.opened; // alter date menu
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };

    /* End of Date */

    if ($rootScope.deliverydate == '' || typeof($rootScope.deliverydate) == 'undefined') {
        $scope.today();
    }
    else {
        $scope.delivery_date = $rootScope.deliverydate;
    }

    $scope.customizedorders = function () {

        $rootScope.deliverydate = document.getElementById('start').value;

        $scope.formInfo = {};

        $scope.formInfo['delivery_date'] = document.getElementById('start').value;
        var serdata = serialize($scope.formInfo);

        console.log(serdata);

        $http({
            url: serviceurl + "checkorders.php",
            method: "POST",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            data: serdata
        }).success(function (data, status, headers, config) {
            $rootScope.$storage.orders = data.list;
            $scope.showfooter = true;
            $rootScope.loader = false;
        }).error(function (data, status, headers, config) {

        });
    };

    $scope.customizedorders();

    $scope.changedate = function () {
        if ($rootScope.checkConnection()) {
            $rootScope.loader = true;
            /* delay because document.getelement doesnot work*/
            setTimeout(function () {
                $rootScope.deliverydate = document.getElementById('start').value;
                console.log($rootScope.deliverydate);
                console.log(document.getElementById('start').value);
                $scope.formInfo = {};

                $scope.formInfo['delivery_date'] = document.getElementById('start').value;
                var serdata = serialize($scope.formInfo);

                console.log(serdata);

                $http({
                    url: serviceurl + "checkorders.php",
                    method: "POST",
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                    data: serdata
                }).success(function (data, status, headers, config) {
                    $rootScope.$storage.orders = data.list;
                    $scope.showfooter = true;
                    $rootScope.loader = false;
                }).error(function (data, status, headers, config) {

                });
            }, 100);
        }
    };

    $scope.getorders = function () {
        $rootScope.deliverydate = document.getElementById('start').value;
        $location.path('/allorders');
    };

    $scope.resetdate = function () {
        $scope.today();
        $rootScope.deliverydate = '';
        $rootScope.orders = '';
        $scope.showfooter = false;
    };

    document.addEventListener("backbutton", onBackKeyDown, false);
});

lsdApp.controller('ModalInstanceCtrl', function ($scope, $modalInstance, $http, customers, $rootScope) {
    if (customers === null) {
        $scope.customers = [];
    }
    else {
        $scope.customers = customers;
    }

    console.log(customers);

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
        $rootScope.customername = '';
    };

    $scope.formInfo = {};

    $scope.submitTheForm = function (item, event) {
        $scope.formInfo['action'] = 'add';
        var serdata = serialize($scope.formInfo);

        console.log(serdata);

        $http({
            url: serviceurl + "customers.php",
            method: "POST",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            data: serdata
        }).success(function (data, status, headers, config) {

            if (data.status == '1') {

                $rootScope.customername = $scope.formInfo.customers_name.toString();
                $rootScope.customerid = data.customers_id;
                $rootScope.customermobile = $scope.formInfo.customers_mobile.toString();
                $rootScope.customercredit = 'N';
                $rootScope.creditamount = '0.0';

                $rootScope.$storage.allcustomers.push({
                    customers_id: data.customers_id,
                    customers_name: $rootScope.customername,
                    customers_mobile: $rootScope.customermobile,
                    has_credit: 'Y',
                    credit_amount: '0.0',
                    has_credit: 'Y',
                    credit_amount: '0.0'
                });


                $modalInstance.dismiss();
            } else if (data.status = '2') {
                navigator.notification.alert('Mobile No. already exists. ', function () {
                }, "Warning", "");

            } else {
            }
        }).error(function (data, status, headers, config) {

        });

    };
});

lsdApp.controller('categoryorderController', function ($scope, $rootScope, $location, $window, $routeParams, $http, $modal) {
    $scope.categoryid = $routeParams.id;
    $scope.categoryname = $routeParams.name;
    $rootScope.title($scope.categoryname);

    if ($rootScope.checkConnection()) {
        $rootScope.loader = true;

        $scope.categoryorders = [];
        if (typeof($rootScope.deliverydate) == 'undefined') {
            $rootScope.deliverydate = '';
        }

        var dataObject = {
            category_id: $scope.categoryid,
            delivery_date: $rootScope.deliverydate
        };

        var serdata = serialize(dataObject);

        console.log(serdata);

        $http({
            url: serviceurl + "categoryorder.php",
            method: "POST",
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: serdata
        }).success(function (data, status, headers, config) {
            console.log(data);
            $rootScope.loader = false;
            $scope.categoryorders = data.list;
        }).error(function (data, status, headers, config) {

        });

    }

    /* modal box */


    $scope.animationsEnabled = true;

    $scope.openimg = function (imgname) {
        if ($rootScope.checkConnection()) {
            var modalInstance = $modal.open({
                animation: $scope.animationsEnabled,
                templateUrl: 'imgModalContent.html',
                controller: 'ImgModalInstanceCtrl',
                size: 'sm',
                resolve: {
                    imgsrc: function () {
                        return imgname;
                    }
                }
            });
        }
    };

    /* end of modal box */
    document.removeEventListener("backbutton", onBackKeyDown, false);

});

lsdApp.controller('ImgModalInstanceCtrl', function ($scope, $modalInstance, $http, $rootScope, imgsrc) {
    $scope.imgsrc = imgsrc;
    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
        $rootScope.customername = '';
    };

});

lsdApp.controller('logoutController', function ($scope, $http, $route, $window, $rootScope, $location) {
    $rootScope.deliverydate = '';
    $rootScope.orders = '';
    $rootScope.$storage.login1 = 0;
    $rootScope.$storage.username = '';
    $rootScope.$storage.customer_id = 0;
    $rootScope.$storage.customer_name = '';
    $rootScope.$storage.credit_amount = '0';
    $rootScope.$storage.has_credit = 'N';
    $rootScope.$storage.order_deliverydate = '';
    $rootScope.$storage.orders = '';
    $location.path("/login");
    document.addEventListener("backbutton", onBackKeyDown, false);

});


